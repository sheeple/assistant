
$clipboard = Win32::Clipboard();

RegisterHotKey("F1", 'DisplayMenu');

$\ = "\x0D\x0A";

sub PopupMenu
{
  return
  [
    ['URL',
      [ ['wykop.pl'   , 'Execute "https://wykop.pl"']
      ]
    ]
  , ['Szablony',
      [ ['select', sub{$clipboard->Set("  select t.*\x0D\x0A    from t\x0D\x0A   where 1 = 1\x0D\x0Aorder by 1\x0D\x0Agroup by 1\x0D\x0A  having 1\x0D\x0A;\x0D\x0A")}]
      ]
    ]
  , ['-']
  , ['Zako�cz ..', 'exit']
  ] ;
}

1;
